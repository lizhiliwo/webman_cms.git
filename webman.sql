/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : webman

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-03-14 08:26:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lizhili_admin
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_admin`;
CREATE TABLE `lizhili_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `auth` int(11) DEFAULT NULL,
  `isopen` int(11) NOT NULL DEFAULT '1',
  `mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_admin
-- ----------------------------
INSERT INTO `lizhili_admin` VALUES ('1', 'admin', '751aff6be33b5649fde05436dc4cb4f7', '1529570040', '1532252345', '0', '1', '超级管理员不能删除和停用');
INSERT INTO `lizhili_admin` VALUES ('2', 'lizhili', '751aff6be33b5649fde05436dc4cb4f7', '1532685192', null, '1', '1', '');

-- ----------------------------
-- Table structure for lizhili_article
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_article`;
CREATE TABLE `lizhili_article` (
  `id` mediumint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `keyword` varchar(10) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `pic` varchar(160) DEFAULT NULL,
  `text` text,
  `state` smallint(6) unsigned DEFAULT '0',
  `click` mediumint(9) DEFAULT '0',
  `zan` mediumint(9) DEFAULT '0',
  `time` int(10) DEFAULT NULL,
  `cateid` mediumint(9) DEFAULT NULL,
  `faid` int(11) DEFAULT '0' COMMENT '发布者id',
  `laiyuan` varchar(255) DEFAULT NULL,
  `click_wai` mediumint(9) DEFAULT '0' COMMENT '展示数据',
  `isopen` tinyint(1) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_article
-- ----------------------------
INSERT INTO `lizhili_article` VALUES ('1', '射雕圣达菲', '', '', '', '/files/20220312173036622c683c5c35c.png', '撒旦法答复圣达菲', '0', '0', '0', '0', '3', '0', '', '1964', '1', '');
INSERT INTO `lizhili_article` VALUES ('2', '第三方阿飞圣达菲', '成功', '发士大夫士大夫圣达菲多少水电费射雕', '水电费', '/files/20220312173350622c68feb6205.png', '发士大夫士大夫圣达菲多少水电费射雕', '1', '0', '0', '1647014400', '3', '0', '撒旦法', '834', '1', 'http://baidu.com');

-- ----------------------------
-- Table structure for lizhili_auth
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_auth`;
CREATE TABLE `lizhili_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lizhili_auth
-- ----------------------------
INSERT INTO `lizhili_auth` VALUES ('1', '其他', '13,20,59', '儿童二天二儿');

-- ----------------------------
-- Table structure for lizhili_cate
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_cate`;
CREATE TABLE `lizhili_cate` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) DEFAULT NULL,
  `fid` tinyint(4) DEFAULT NULL,
  `type` tinyint(3) DEFAULT NULL COMMENT '1代表是列表，2代表是单页，3代表图片列表，4代表打开链接',
  `keyword` varchar(255) DEFAULT NULL COMMENT '栏目关键字',
  `desc` varchar(255) DEFAULT NULL,
  `text` text COMMENT '单页的数据',
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_cate
-- ----------------------------
INSERT INTO `lizhili_cate` VALUES ('1', '士大夫送达', '0', '1', '', '', '', '5', '1647071212', '1647071212', '0', '');
INSERT INTO `lizhili_cate` VALUES ('2', '隐私协议', '1', '2', '', '', '水电费第三方第三方第三方第三方第三方', '0', '1647071224', '1647071224', '1', '');
INSERT INTO `lizhili_cate` VALUES ('3', '水电费水电费第三方', '0', '4', '', '', '', '0', '1647074876', '1647071245', '1', 'http://baidu.com');
INSERT INTO `lizhili_cate` VALUES ('4', '水电费', '0', '4', '', '', '', '0', '1647071414', '1647071414', '1', '');

-- ----------------------------
-- Table structure for lizhili_cms
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_cms`;
CREATE TABLE `lizhili_cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `iswo` tinyint(1) DEFAULT '1',
  `baidu_token` varchar(255) DEFAULT NULL COMMENT '百度的token，seo使用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_cms
-- ----------------------------
INSERT INTO `lizhili_cms` VALUES ('1', '<p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">感谢您一年来对我们的支持和包容。为了更好的服务大家，在2018年6月份，我们全新发布了后台管理系统版本。我们的发布离不开广大用户给出的建议和意见。我们整合了更多优秀插件；优化了框架的体积。当然相比目前行业其他管理系统还有很多不足。但初心不改，实实在在把事做好，做用户最喜欢的框架。更好为客户服务。</p><p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">我们在2018年版本上面，先进行了，大量的技术更新，包括了秒杀，团购，即时通讯，购物，等等功能的扩展。然后在2019年的9月和11月份，我们又进行了重构，大量的精简了原始代码，把原始的一些插件进行了替换，删除没有必要的程序增加水印缩略图等功能，速度是2018年第一版的3倍以上。从最早网站开发，到现在我们已经经历过了6个年头，我们经历过的项目数百个，每一次修改后台我们都抱着不忘初心的态度，努力的写好每一句代码，希望我们的努力，可以得到您的认可。你们的肯定就是对我们最大支持！</p><p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">2020年，添加了广告和水印的判断，修复了bug。添加了数据备份还原功能。添加了关闭网站后的302重定向。5月份添加了后台动态修改菜单功能。6月修改了sql逻辑，添加了广告分类等等。添加了商品的操作。添加了大量的功能。7月修改分销的逻辑，已经发现的bug。8月份添加面包屑等前台功能。模仿dede添加多级循环等标签！</p><p style=\"overflow-wrap: break-word; margin-top: 0px; margin-bottom: 10px; padding: 0px; color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; white-space: normal; text-indent: 20px;\">2020年12月，进行大量修改bug，更新了下载，搜索，SEO，等等功能，更新手册！2021年2月份进行了，修改已知bug，修改了操作手册！2021年6月，修改已知bug，添加多个类库组件。适配php8.0版本。8月份，又进行大量修改，包括上传视频，合并标签，修改手册等功能。10月份添加一键生成模块功能，大大减少了代码开发，<span style=\"color: rgb(51, 51, 51); font-family: &quot;Microsoft Yahei&quot;, &quot;Hiragino Sans GB&quot;, &quot;Helvetica Neue&quot;, Helvetica, tahoma, arial, &quot;WenQuanYi Micro Hei&quot;, Verdana, sans-serif, 宋体; font-size: 14px; text-indent: 20px;\">非常实用</span>！！！11月，修改了认证和SEO，更好用了！！！又添加了定时器和消息队列的使用等。如果有使用上面问题可以联系我，lizhilimaster@163.com。</p><p><br/></p>', '1', '');

-- ----------------------------
-- Table structure for lizhili_config
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_config`;
CREATE TABLE `lizhili_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `shuo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_config
-- ----------------------------
INSERT INTO `lizhili_config` VALUES ('1', 'watermark', '0', '水印');
INSERT INTO `lizhili_config` VALUES ('2', 'shui_weizhi', '9', '水印位置具体看手册');
INSERT INTO `lizhili_config` VALUES ('3', 'shui_neirong', '李志立 lizhilimaster@163.com', '水印内容');
INSERT INTO `lizhili_config` VALUES ('4', 'thumbnail', '0', '缩率图');
INSERT INTO `lizhili_config` VALUES ('5', 't_w', '10001', '缩略图宽');
INSERT INTO `lizhili_config` VALUES ('6', 't_h', '300', '缩略图高');
INSERT INTO `lizhili_config` VALUES ('7', 'shui_zihao', '18', '水印字号');
INSERT INTO `lizhili_config` VALUES ('8', 'shui_yanse', '#ffffff', '水印颜色');
INSERT INTO `lizhili_config` VALUES ('9', 'is_ya', '1', '是否开启压缩图片');
INSERT INTO `lizhili_config` VALUES ('10', 'ya_w', '1000', '压缩后的图片大小');

-- ----------------------------
-- Table structure for lizhili_log
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_log`;
CREATE TABLE `lizhili_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `ip` char(15) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0' COMMENT '是否成功',
  `password` varchar(1000) DEFAULT NULL,
  `is_cheng` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_log
-- ----------------------------
INSERT INTO `lizhili_log` VALUES ('2', 'sdf', '127.0.0.1', '1645868662', '0', 'c2FkZmxpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('3', 'admin', '127.0.0.1', '1645868693', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('4', 'admin', '127.0.0.1', '1645868720', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('5', 'admin', '127.0.0.1', '1645868724', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('6', 'admin', '127.0.0.1', '1645868743', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('7', 'admin', '127.0.0.1', '1645868791', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('8', 'admin', '127.0.0.1', '1646014682', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('9', 'admin', '127.0.0.1', '1646014700', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('10', 'admin', '127.0.0.1', '1646131882', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('11', 'admin', '127.0.0.1', '1646381144', '0', 'bGl6aGlsaWxpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('12', 'admin', '127.0.0.1', '1646381159', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('13', 'admin', '127.0.0.1', '1646446287', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('14', 'admin', '127.0.0.1', '1646734321', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('15', 'admin', '127.0.0.1', '1646815708', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('16', 'admin', '127.0.0.1', '1646874174', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('17', 'admin', '127.0.0.1', '1646874193', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('18', 'admin', '127.0.0.1', '1646882386', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('19', 'admin', '127.0.0.1', '1646885615', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('20', 'admin', '127.0.0.1', '1646896885', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('21', 'admin', '127.0.0.1', '1646904381', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('22', 'admin', '127.0.0.1', '1646963212', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('23', 'admin', '127.0.0.1', '1646963229', '0', 'bGl6aGlsaWxpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('24', 'admin', '127.0.0.1', '1646963241', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('25', 'admin', '127.0.0.1', '1646968744', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('26', 'admin', '127.0.0.1', '1646981125', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('27', 'lizhili', '127.0.0.1', '1646988932', '2', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('28', 'admin', '127.0.0.1', '1646989582', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('29', 'admin', '127.0.0.1', '1646989616', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('30', 'admin', '127.0.0.1', '1646991446', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('31', 'admin', '127.0.0.1', '1646991802', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('32', 'admin', '127.0.0.1', '1647052148', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('33', 'admin', '127.0.0.1', '1647071755', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('34', 'admin', '127.0.0.1', '1647071772', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');
INSERT INTO `lizhili_log` VALUES ('35', 'admin', '127.0.0.1', '1647073014', '0', 'bGl6aGlsaTEyM2xpemhpbGk=', '0');
INSERT INTO `lizhili_log` VALUES ('36', 'admin', '127.0.0.1', '1647073028', '1', 'bGl6aGlsaTEyM2xpemhpbGk=', '1');

-- ----------------------------
-- Table structure for lizhili_pilot_list
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_pilot_list`;
CREATE TABLE `lizhili_pilot_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` smallint(6) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `fid` int(11) DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  `pn_id` int(11) DEFAULT '1' COMMENT '头部导航',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='后台侧面导航';

-- ----------------------------
-- Records of lizhili_pilot_list
-- ----------------------------
INSERT INTO `lizhili_pilot_list` VALUES ('1', '1', '栏目管理', '0', 'fa-align-center', '没有地址', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('3', '3', '管理员管理', '0', 'fa-arrows-alt', '', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('4', '4', '系统管理', '0', 'fa-anchor', '', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('5', '6', '登陆管理', '0', 'fa-align-justify', '', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('8', '1', '资讯管理', '0', 'fa-arrow-up', '', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('13', '0', '栏目管理', '1', '', 'cate/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('16', '0', '角色管理', '3', '', 'auth/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('18', '0', '管理员列表', '3', null, 'admin/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('19', '0', '设置管理', '4', null, 'system/index', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('20', '0', '系统设置', '4', null, 'system/show', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('23', '0', '顶部导航', '31', null, 'pilot/nav', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('25', '0', '资讯管理', '8', null, 'article/index', '1', '2');
INSERT INTO `lizhili_pilot_list` VALUES ('31', '5', '导航设置', '0', 'fa-birthday-cake', '', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('32', '0', '侧面导航', '31', null, 'pilot/lit', '1', '1');
INSERT INTO `lizhili_pilot_list` VALUES ('59', '0', '查看日志', '5', '', 'log/index', '1', '1');

-- ----------------------------
-- Table structure for lizhili_pilot_nav
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_pilot_nav`;
CREATE TABLE `lizhili_pilot_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` smallint(6) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='后台头部导航';

-- ----------------------------
-- Records of lizhili_pilot_nav
-- ----------------------------
INSERT INTO `lizhili_pilot_nav` VALUES ('1', '0', '网站配置', '1');
INSERT INTO `lizhili_pilot_nav` VALUES ('2', '0', '内容管理', '1');

-- ----------------------------
-- Table structure for lizhili_system
-- ----------------------------
DROP TABLE IF EXISTS `lizhili_system`;
CREATE TABLE `lizhili_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cnname` varchar(100) DEFAULT NULL,
  `enname` varchar(100) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1',
  `value` varchar(1000) DEFAULT NULL,
  `kxvalue` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `st` tinyint(3) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lizhili_system
-- ----------------------------
INSERT INTO `lizhili_system` VALUES ('1', '网站名称', 'webname', '1', 'unicms内容管理系统', '', '网站名称', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('2', '关键词', 'keyword', '1', 'unicms内容管理系统,cms,lizhili', '', '网站关键字', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('3', '描述', 'miaoshu', '1', '一款优秀的内容管理系统', '', '网站描述', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('4', '底部版权信息', 'copyright', '1', '', '', '网站版权信息', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('5', '备案号', 'no', '1', '', '', '网站备案号', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('6', '统计代码', 'statistics', '2', '<script>var _hmt = _hmt || [];(function() {  var hm = document.createElement(\"script\");  hm.src = \"https://hm.baidu.com/hm.js?e7373a584fcd336e9a2e3053c6f0a392\";  var s = document.getElementsByTagName(\"script\")[0];   s.parentNode.insertBefore(hm, s);})();</script>', '', '网站统计代码', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('7', '网站状态', 'value', '3', '关闭', '开启,关闭', '网站的状态', '1', null, '1567676416');
INSERT INTO `lizhili_system` VALUES ('8', '闭站重定向', 'redirect', '1', 'http://down.linglukeji.com/', '', '', '1', '1585987185', '1585987185');
INSERT INTO `lizhili_system` VALUES ('9', '屏蔽词', 'ping', '2', '她妈|它妈|他妈|你妈|去死|贱人|1090tv|10jil|21世纪中国基金会|2c8|3p|4kkasi|64惨案|64惨剧|64学生运动|64运动|64运动民國|89惨案|89惨剧|89学生运动|89运动|adult|asiangirl|avxiu|av女|awoodong|A片|bbagoori|bbagury|bdsm|binya|bitch|bozy|bunsec|bunsek|byuntae|B样|fa轮|fuck|ＦｕｃΚ|gay|hrichina|jiangzemin|j女|kgirls|kmovie|lihongzhi|MAKELOVE|NND|nude|petish|playbog|playboy|playbozi|pleybog|pleyboy|q奸|realxx|s2x|sex|shit|sorasex|tmb|TMD|tm的|tongxinglian|triangleboy|UltraSurf|unixbox|ustibet|voa|admin|lizhili|manage', '11112222', '水电费水电费1133', '0', null, '1647059419');
INSERT INTO `lizhili_system` VALUES ('10', 'logo', 'logo', '4', '/files/20220312122842622c217ab3d32.png', '', '', '0', '1647056316', '1647059414');
