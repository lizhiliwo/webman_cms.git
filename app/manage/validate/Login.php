<?php
namespace app\manage\validate;

use Tinywan\Validate\Validate;

class Login extends Validate
{
    protected $rule =   [
        'username'  => 'require',
        'password'   => 'require',
        'captcha' => 'require'
    ];

    protected $message  =   [
        'username.require' => '用户名必须填写',
        'password.require' => '密码必须填写',
        'captcha.require' => '验证码必须填写',
    ];
    protected $scene = [
        'edit'  =>  ['age'],
    ];

}