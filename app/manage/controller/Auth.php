<?php

namespace app\manage\controller;

use support\Request;
use app\manage\controller\Base;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Db;

class Auth extends Base
{
    public function index(Request $request)
    {
        return view('auth/index', [

        ]);
    }
    public function add(Request $request)
    {
        return view('auth/add', [
            'auth'=>Db::name('pilot_list')->where('fid','<>',0)->field('title,id')->select()
        ]);
    }
    public function edit(Request $request)
    {
        return view('auth/edit', [
            'auth'=>Db::name('pilot_list')->where('fid','<>',0)->field('title,id')->select(),
            'data'=>Db::name('auth')->find($request->input('id'))
        ]);
    }
    public function api(Request $request){
        $data=$request->all();

        if($data['type']=='index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('auth')->count();
            $info=Db::name('auth')->where('title','like','%'.$key.'%')->select()->toArray();
            foreach ($info as $k=>$v){
                $wo=Db::name('pilot_list')->where('id','in',$v['auth'])->column('title');
                $info[$k]['auth']=implode(",", $wo);
            }

            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }
        if($data['type']=='add'){
            $info=$data['data'];
            Db::name('auth')->insert($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='edit'){
            $info=$data['data'];
            Db::name('auth')->where('id',$data['id'])->update($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        //批量删除
        if($data['type']=='del_all'){
            $arr=$data['data'];

//            if(in_array(1,explode(",", $arr))){
//                return json([
//                    'code'=>1,
//                    'msg'=>'超级管理员不能删除！',
//                ]);
//            }

            Db::name('auth')->where('id','in',$arr)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //删除一个
        if($data['type']=='del_one'){
            $one=$data['data'];
            Db::name('auth')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }





        return json([
            'code'=>2,
            'msg'=>'调用失败'
        ]);

    }
}
