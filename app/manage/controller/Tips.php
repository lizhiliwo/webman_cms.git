<?php
namespace app\manage\controller;

use app\manage\controller\Base;
use support\Request;
use Gregwar\Captcha\CaptchaBuilder;

class Tips extends Base
{
    public function index(Request $request)
    {
        $data=$request->all();
        $data['wait']=5;
        return view('index', $data);
    }

}
