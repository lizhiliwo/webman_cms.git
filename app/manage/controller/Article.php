<?php

namespace app\manage\controller;

use support\Request;
use app\manage\controller\Base;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Db;

class Article extends Base
{
    public function index(Request $request)
    {
        return view('article/index', [

        ]);
    }
    public function add(Request $request)
    {
        return view('article/add', [
            'cate'=>(new \app\manage\model\Cate())->tree()
        ]);
    }
    public function edit(Request $request)
    {
        $data=Db::name('article')->find($request->input('id'));
        return view('article/edit', [
            'cate'=>(new \app\manage\model\Cate())->tree(),
            'data'=>$data,
        ]);
    }
    public function update(Request $request)
    {
        $data=$request->all();
        if(!isset($data['lizhili']) or $data['lizhili']!='123456'){
            return json([
                'code'=>1,
                'msg'=>'接口错误'
            ]);
        }
        $file = $request->file('file');
        if ($file && $file->isValid()) {
            $wei='/files/'.date('YmdHis').uniqid().'.'.$file->getUploadExtension();
            $file->move(public_path().$wei);
            return json(['code' => 0, 'msg' => '成功','data'=>[
                'src'=>$wei
            ]]);
        }
        return json(['code' => 1, 'msg' => '上传失败']);
    }
    public function api(Request $request){
        $data=$request->all();

        if($data['type']=='index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('article')->count();
            $info= Db::name('article')->where('title','like','%'.$key.'%')->page($data['page'],$data['limit'])->select()->toArray();
            foreach ($info as $k=>&$v){
                $v['cateid']=Db::name('cate')->where('id',$v['cateid'])->value('title');
            }
            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }

        if($data['type']=='add'){
            $info=$data['data'];
            if(isset($info['state'])){
                $info['state']=1;
            }else{
                $info['state']=0;
            }
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            if(empty($info['time'])){
                $info['time']=time();
            }else{
                $info['time']=strtotime($info['time']);
            }
            if ($info['desc']=='') {
                $info['desc']=mb_substr(preg_replace('/\&nbsp;/', '', strip_tags($info['text'])), 0, 80);
            }
           Db::name('article')->strict(false)->insert($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='edit'){
            $info=$data['data'];
            if(isset($info['state'])){
                $info['state']=1;
            }else{
                $info['state']=0;
            }
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            if(empty($info['time'])){
                $info['time']=time();
            }else{
                $info['time']=strtotime($info['time']);
            }
            if ($info['desc']=='') {
                $info['desc']=mb_substr(preg_replace('/\&nbsp;/', '', strip_tags($info['text'])), 0, 80);
            }

            Db::name('article')->strict(false)->where('id',$data['id'])->update($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }

        //删除一个
        if($data['type']=='del_one'){
            $one=$data['data'];
            Db::name('article')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //开启
        if($data['type']=='open_one'){
            $one=$data['data'];
            Db::name('article')->where('id',$one)->update(['isopen'=>1]);
            return json([
                'code'=>0,
                'msg'=>'开启成功',
            ]);
        }
        //停用
        if($data['type']=='stop_one'){
            $one=$data['data'];
            Db::name('article')->where('id',$one)->update(['isopen'=>0]);
            return json([
                'code'=>0,
                'msg'=>'关闭成功',
            ]);
        }
        return json([
            'code'=>2,
            'msg'=>'调用失败'
        ]);

    }
    
}
