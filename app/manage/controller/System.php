<?php

namespace app\manage\controller;

use support\Request;
use app\manage\controller\Base;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Db;

class System extends Base
{
    public function index(Request $request)
    {
        return view('system/index', [

        ]);
    }
    public function add(Request $request)
    {
        return view('system/add', [

        ]);
    }
    public function edit(Request $request)
    {
        $data=Db::name('system')->find($request->input('id'));
        return view('system/edit', [
            'data'=>$data,
        ]);
    }
    public function show(Request $request)
    {
        $data=Db::name('system')->select();
        return view('system/show', [
            'data'=>$data,
        ]);
    }
    public function update(Request $request)
    {
        $data=$request->all();
        if(!isset($data['lizhili']) or $data['lizhili']!='123456'){
            return json([
                'code'=>1,
                'msg'=>'接口错误'
            ]);
        }
        $file = $request->file('file');
        if ($file && $file->isValid()) {
            $wei='/files/'.date('YmdHis').uniqid().'.'.$file->getUploadExtension();
            $file->move(public_path().$wei);
            return json(['code' => 0, 'msg' => '成功','data'=>$wei]);
        }
        return json(['code' => 1, 'msg' => '上传失败']);
    }
    public function api(Request $request){
        $data=$request->all();

        if($data['type']=='index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('system')->count();
            $info=Db::name('system')->where('cnname','like','%'.$key.'%')->select();
            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }

        if($data['type']=='add'){
            $info=$data['data'];
            if(Db::name('system')->where('enname',$info['enname'])->find()){
                return json([
                    'code'=>1,
                    'msg'=>'已经存在不能重复添加！',
                ]);
            }

            if($info['kxvalue']){
                $info['kxvalue']=str_replace("，", ",", $info['kxvalue']);
                $info['value']=explode(",", $info['kxvalue'])[0];
            }
            $info['create_time']=time();
            $info['update_time']=time();
            Db::name('system')->insert($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='edit'){
            $info=$data['data'];
            if($info['kxvalue']){
                $info['kxvalue']=str_replace("，", ",", $info['kxvalue']);
                $info['value']=explode(",", $info['kxvalue'])[0];
            }
            $info['update_time']=time();
            Db::name('system')->where('id',$data['id'])->update($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }

        //删除一个
        if($data['type']=='del_one'){
            $one=$data['data'];
            Db::name('system')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //修改内容
         if($data['type']=='show_edit'){
             $info=$data['data'];
             if(!empty($info["redirect"]) and substr($info["redirect"],0,4) != 'http'){
                 return json([
                     'code'=>1,
                     'msg'=>'重定向网址不正确!'
                 ]);
             }
            foreach ($info as $k=>$v){
                Db::name('system')->where('enname',$k)->update(['value'=>$v]);
            }
             return json([
                 'code'=>0,
                 'msg'=>'成功'
             ]);
         }
        return json([
            'code'=>2,
            'msg'=>'调用失败'
        ]);

    }
    
}
