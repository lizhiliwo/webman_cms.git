<?php
namespace app\manage\controller;
use support\Request;
use support\View;
use think\facade\Db;
class Base
{

    public function beforeAction(Request $request)
    {
        View::assign([
            'system' => Db::name('system')->column('value','enname')
        ]);
        // 若果想终止执行Action就直接返回Response对象，不想终止则无需return
        // return response('终止执行Action');
    }


    public function afterAction(Request $request, $response)
    {
       // echo 'afterAction';
        // 如果想串改请求结果，可以直接返回一个新的Response对象
        // return response('afterAction');
    }


}