<?php
namespace app\manage\controller;

use app\manage\controller\Base;
use app\manage\model\PilotList;
use support\Request;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Db;
use think\Model;

class Pilot extends Base
{
    public function lit(Request $request)
    {
        return view('pilot/lit', [

        ]);
    }
    public function listadd(Request $request)
    {
        return view('pilot/listadd', [
            'pn'=>Db::name('pilot_nav')->where('isopen',1)->select()
        ]);
    }
    public function listedit(Request $request)
    {
        $data=Db::name('pilot_list')->find($request->input('id'));
        return view('pilot/listedit', [
            'pn'=>Db::name('pilot_nav')->where('isopen',1)->select(),
            'data'=>$data,
            'list'=>Db::name('pilot_list')->where('pn_id',$data['pn_id'])->where('isopen',1)->select()
        ]);
    }
    public function nav(Request $request)
    {
        return view('pilot/nav', [

        ]);
    }
    public function navadd(Request $request)
    {
        return view('pilot/navadd', [

        ]);
    }
    public function navedit(Request $request)
    {
        return view('pilot/navedit', [
            'data'=>Db::name('pilot_nav')->find($request->input('id'))
        ]);
    }
    public function api(Request $request){
        $data=$request->all();

        if($data['type']=='lit_index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('pilot_list')->count();
            if($key){
                $info=Db::name('pilot_list')->where('title','like','%'.$key.'%')->order('sort asc')->select();
            }else{
                $p=new PilotList();
                $info=$p->tree();
            }

            foreach ($info as &$v){
                $v['pn_id']=Db::name('pilot_nav')->where('id',$v['pn_id'])->value('title');
            }
            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }
        //list_add_select
        if($data['type']=='list_add_select'){
            $info=Db::name('pilot_list')->where('pn_id',$data['pn_id'])->where('isopen',1)->select();
            return json([
                'code'=>0,
                'data'=>$info
            ]);
        }

        if($data['type']=='list_add'){
            $info=$data['data'];
//            if(!$info['icon']){
//                $info['icon']='fa-adjust';
//            }
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            Db::name('pilot_list')->insert($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='list_edit'){
            $info=$data['data'];
//            if(!$info['icon']){
//                $info['icon']='fa-adjust';
//            }
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            dump($data,$info);
            Db::name('pilot_list')->where('id',$data['id'])->update($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        //批量删除
        if($data['type']=='list_del_all'){
            $arr=$data['data'];
            foreach ($arr as $v){
                if(Db::name('pilot_list')->where('fid',$v)->find()){
                    return json([
                        'code'=>1,
                        'msg'=>'拥有下一级不能删除！',
                    ]);
                }
            }

            Db::name('pilot_list')->where('id','in',$arr)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //删除一个
        if($data['type']=='list_del_one'){
            $one=$data['data'];
            if(Db::name('pilot_list')->where('fid',$one)->find()){
                return json([
                    'code'=>1,
                    'msg'=>'拥有下一级不能删除！',
                ]);
            }
            Db::name('pilot_list')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //修改数据
        if($data['type']=='list_edit_one'){
            $one=json_decode($data['data'],true);
            Db::name('pilot_list')->where('id',$one['id'])->update(['sort'=>$one['sort']]);
            return json([
                'code'=>0,
                'msg'=>'修改成功',
            ]);
        }
        //开启
        if($data['type']=='list_open_one'){
            $one=$data['data'];
            Db::name('pilot_list')->where('id',$one)->update(['isopen'=>1]);
            return json([
                'code'=>0,
                'msg'=>'开启成功',
            ]);
        }
        //停用
        if($data['type']=='list_stop_one'){
            $one=$data['data'];
            Db::name('pilot_list')->where('id',$one)->update(['isopen'=>0]);
            return json([
                'code'=>0,
                'msg'=>'关闭成功',
            ]);
        }


        if($data['type']=='nav_index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('pilot_nav')->count();
            $info=Db::name('pilot_nav')->where('title','like','%'.$key.'%')->page($data['page'],$data['limit'])->order('sort asc')->select();
            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }
        if($data['type']=='nav_add'){
            $info=$data['data'];
            Db::name('pilot_nav')->insert([
                'title'=>$info['title']
            ]);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='nav_edit'){
            $info=$data['data'];
            Db::name('pilot_nav')->where('id',$data['id'])->update([
                'title'=>$info['title']
            ]);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        //批量删除
        if($data['type']=='nav_del_all'){
            $arr=$data['data'];
            Db::name('pilot_nav')->where('id','in',$arr)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //删除一个
        if($data['type']=='nav_del_one'){
            $one=$data['data'];
            Db::name('pilot_nav')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //修改数据
         if($data['type']=='nav_edit_one'){
             $one=json_decode($data['data'],true);
             Db::name('pilot_nav')->save($one);
             return json([
                 'code'=>0,
                 'msg'=>'修改成功',
             ]);
         }
        //开启
        if($data['type']=='nav_open_one'){
            $one=$data['data'];
            Db::name('pilot_nav')->where('id',$one)->update(['isopen'=>1]);
            return json([
                'code'=>0,
                'msg'=>'开启成功',
            ]);
        }
        //停用
        if($data['type']=='nav_stop_one'){
            $one=$data['data'];
            Db::name('pilot_nav')->where('id',$one)->update(['isopen'=>0]);
            return json([
                'code'=>0,
                'msg'=>'关闭成功',
            ]);
        }
        return json([
            'code'=>2,
            'msg'=>'调用失败'
        ]);

    }


}
