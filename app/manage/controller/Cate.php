<?php

namespace app\manage\controller;

use support\Request;
use app\manage\controller\Base;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Db;

class Cate extends Base
{
    public function index(Request $request)
    {
        return view('cate/index', [

        ]);
    }
    public function add(Request $request)
    {
        return view('cate/add', [
            'fid'=>Db::name('cate')->where('fid',0)->select()
        ]);
    }
    public function edit(Request $request)
    {
        $data=Db::name('cate')->find($request->input('id'));
        return view('cate/edit', [
            'fid'=>Db::name('cate')->where('fid',0)->select(),
            'data'=>$data,
        ]);
    }
    public function update(Request $request)
    {
        $data=$request->all();
        if(!isset($data['lizhili']) or $data['lizhili']!='123456'){
            return json([
                'code'=>1,
                'msg'=>'接口错误'
            ]);
        }
        $file = $request->file('file');
        if ($file && $file->isValid()) {
            $wei='/files/'.date('YmdHis').uniqid().'.'.$file->getUploadExtension();
            $file->move(public_path().$wei);
            return json(['code' => 0, 'msg' => '成功','data'=>[
                'src'=>$wei
            ]]);
        }
        return json(['code' => 1, 'msg' => '上传失败']);
    }
    public function api(Request $request){
        $data=$request->all();

        if($data['type']=='index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('cate')->count();
            $info= (new \app\manage\model\Cate())->tree($key);
            foreach ($info as $k=>&$v){
                $v['fid']=Db::name('cate')->where('id',$v['fid'])->value('title') ?? '顶级栏目';
            }
            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }

        if($data['type']=='add'){
            $info=$data['data'];
            if ($info['type']==4 and substr($info['url'],0,4)!='http') {
                return json([
                    'code'=>1,
                    'msg'=>'url地址不正确',
                ]);
            }
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            $info['create_time']=time();
            $info['update_time']=time();
           Db::name('cate')->strict(false)->insert($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='edit'){
            $info=$data['data'];
            if ($info['type']==4 and substr($info['url'],0,4)!='http') {
                return json([
                    'code'=>1,
                    'msg'=>'url地址不正确',
                ]);
            }
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            $info['update_time']=time();

            Db::name('cate')->strict(false)->where('id',$data['id'])->update($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        //批量删除
        if($data['type']=='del_all'){
            $arr=$data['data'];
            if(Db::name('cate')->where('fid','in',$arr)->find()){
                return json([
                    'code'=>1,
                    'msg'=>'拥有下级栏目不能直接删除！',
                ]);
            }
            Db::name('cate')->where('id','in',$arr)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //修改数据
        if($data['type']=='edit_one'){
            $one=json_decode($data['data'],true);
            Db::name('cate')->where('id',$one['id'])->update(['sort'=>$one['sort']]);
            return json([
                'code'=>0,
                'msg'=>'修改成功',
            ]);
        }
        //删除一个
        if($data['type']=='del_one'){
            $one=$data['data'];
            if(Db::name('cate')->where('fid',$one)->find()){
                return json([
                    'code'=>1,
                    'msg'=>'拥有下级栏目不能直接删除！',
                ]);
            }
            Db::name('cate')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //开启
        if($data['type']=='open_one'){
            $one=$data['data'];
            Db::name('cate')->where('id',$one)->update(['isopen'=>1]);
            return json([
                'code'=>0,
                'msg'=>'开启成功',
            ]);
        }
        //停用
        if($data['type']=='stop_one'){
            $one=$data['data'];
            Db::name('cate')->where('id',$one)->update(['isopen'=>0]);
            return json([
                'code'=>0,
                'msg'=>'关闭成功',
            ]);
        }
        return json([
            'code'=>2,
            'msg'=>'调用失败'
        ]);

    }
    
}
