<?php

namespace app\manage\controller;

use support\Request;
use app\manage\controller\Base;
use Gregwar\Captcha\CaptchaBuilder;
use think\facade\Db;

class Admin extends Base
{
    public function index(Request $request)
    {
        return view('admin/index', [

        ]);
    }
    public function add(Request $request)
    {
        return view('admin/add', [
            'auth'=>Db::name('auth')->select()
        ]);
    }
    public function edit(Request $request)
    {
        $data=Db::name('admin')->find($request->input('id'));
        return view('admin/edit', [
            'auth'=>Db::name('auth')->select(),
            'data'=>$data,
        ]);
    }

    public function api(Request $request){
        $data=$request->all();

        if($data['type']=='index'){
            $key= isset($data['key']) ? $data['key'] :'';
            $count=Db::name('admin')->count();
            $info=Db::name('admin')->where('username','like','%'.$key.'%')->select();
            foreach ($info as &$v){
                $v['auth']=Db::name('auth')->where('id',$v['auth'])->value('title');
            }
            return json([
                'code'=>0,
                'msg'=>'ok',
                'count'=>$count,
                'data'=>$info
            ]);
        }

        if($data['type']=='add'){
            $info=$data['data'];
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            $info['password']=md5(substr(md5($info['password']), 0, 25) . 'lizhili');
           // Db::name('admin')->insert($info);
            \app\manage\model\Admin::create($info);

            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        if($data['type']=='edit'){
            $info=$data['data'];
            if(isset($info['isopen'])){
                $info['isopen']=1;
            }else{
                $info['isopen']=0;
            }
            if($info['password']){
                $info['password']=md5(substr(md5($info['password']), 0, 25) . 'lizhili');
            }else{
                unset($info['password']);
            }

            Db::name('admin')->where('id',$data['id'])->update($info);
            return json([
                'code'=>0,
                'msg'=>'添加成功',
            ]);
        }
        //批量删除
        if($data['type']=='del_all'){
            $arr=$data['data'];
            if(in_array(1,$arr)){
                return json([
                    'code'=>1,
                    'msg'=>'不能删除总管理员！',
                ]);
            }
            Db::name('admin')->where('id','in',$arr)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //删除一个
        if($data['type']=='del_one'){
            $one=$data['data'];
            if($one==1){
                return json([
                    'code'=>1,
                    'msg'=>'不能删除总管理员！',
                ]);
            }
            Db::name('admin')->where('id',$one)->delete();
            return json([
                'code'=>0,
                'msg'=>'删除成功',
            ]);
        }
        //开启
        if($data['type']=='open_one'){
            $one=$data['data'];
            Db::name('admin')->where('id',$one)->update(['isopen'=>1]);
            return json([
                'code'=>0,
                'msg'=>'开启成功',
            ]);
        }
        //停用
        if($data['type']=='stop_one'){
            $one=$data['data'];
            Db::name('admin')->where('id',$one)->update(['isopen'=>0]);
            return json([
                'code'=>0,
                'msg'=>'关闭成功',
            ]);
        }
        return json([
            'code'=>2,
            'msg'=>'调用失败'
        ]);

    }
    
}
