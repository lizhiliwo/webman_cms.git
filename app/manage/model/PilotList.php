<?php

namespace app\manage\model;
use think\facade\Db;
use think\Model;

class PilotList extends Model
{
    public function tree(){
        $res=null;
        $res = $this
            ->alias('pl')
            ->Join('pilot_nav pn','pl.pn_id = pn.id','left')
            ->field('pl.*,pn.title as pname')
            ->order('pl.sort ASC')->select();
        return $this->sort($res,0,0,0);
    }
    public function sort($res,$fid=0,$level=0,$model=0){
        static $arr=[];//这样需要学习一下
        if($model==0){
            $arr=[];
        }
        foreach ($res as $key => $value) {
            if($value['fid']==$fid){
                $value['level']=$level;
                $arr[]=$value;
                $this->sort($res,$value['id'],$level+1,1);
            }
        }
        return $arr;
    }

}