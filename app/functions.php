<?php
/**
 * Here is your custom functions.
 */
//打印
function dump(){
    if(func_num_args()==1){
        var_dump(func_get_args()[0]);
    }elseif (func_num_args()>=1){
        var_dump(func_get_args());
    }
}
//自己定义表单令牌
function token(){
    $time=md5(time());
    session(['__token__'=>$time]);
    return "<input type='hidden' name='__token__' value='$time'>";
}
//成功跳转
function tips($code=0,$msg='',$url='',$ts=0){
    return redirect("/manage/tips?code=$code&msg=$msg&url=$url&ts=$ts");
}
