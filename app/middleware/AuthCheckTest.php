<?php
namespace app\middleware;

use Webman\MiddlewareInterface;
use Webman\Http\Response;
use Webman\Http\Request;

class AuthCheckTest implements MiddlewareInterface
{
    public function process(Request $request, callable $next) : Response
    {
        $session = $request->session();
        $c=$request->controller;
        $a=$request->action;
        if (!($c=="app\manage\controller\Index" and ($a=="login" or $a=='captcha')) and !($c=="app\manage\controller\Tips" and $a=='index') and !$session->get('uid')) {
            return redirect('/manage/index/login');
        }
        return $next($request);
    }
}
